﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;

using ASAM.XIL.Interfaces.Testbench;
using ASAM.XIL.Interfaces.Testbench.MAPort;
using ASAM.XIL.Interfaces.Testbench.Common.ValueContainer;
using ASAM.XIL.Implementation.TestbenchFactory.Testbench;
using ASAM.XIL.Implementation.Testbench.Common.ValueContainer;
using ASAM.XIL.Interfaces.Testbench.NetworkPort;
using DrimcaveXilAPI;

namespace TestTcpipClient
{
    class Program
    {

        static void Main(string[] args)
        {
            INetworkPort sNetPort;
            IMAPort sMAPort;
            string sManifestFolder = "../../../XIL API Configuration";
            string sVendorName = "DRIMAES";
            string sProductName = "DRIMCAVE";
            string sProductVersion = "1.1.0.0";
            try
            {
                TestbenchFactory factory = new TestbenchFactory(sManifestFolder);
                ITestbench testbench = factory.CreateVendorSpecificTestbench(sVendorName, sProductName, sProductVersion);
                sNetPort = testbench.NetworkPortFactory.CreateNetworkPort("my");
                sNetPort.Start();
                Console.WriteLine($"{sNetPort.Name} , {sNetPort.State}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return;
            }
            while(true)
            {
                if (sNetPort.State == ASAM.XIL.Interfaces.Testbench.NetworkPort.Enum.NPState.eRUNNING)
                    break;
                Console.WriteLine("wait running");
                Thread.Sleep(1000);
            }
            Console.WriteLine("Connected Server");
            IChannel ch = sNetPort.Clusters[0].Channels[0];
            ch.StartReceiving();
            int count = 0;
            while (true)
            {
                foreach (IFrame f in ch.Frames)
                {
                    if (f.Receiver.IsActive)
                    {
                        IFrameValue fvData = f.Receiver.SingleReceive(0);

                        byte[] dd = new byte[8];
                        fvData.DataBytes.CopyTo(dd, 0);
                        //Console.WriteLine($"Recevie[{fvData.DataBytes[0]}][{fvData.DataBytes[1]}][{fvData.DataBytes[2]}][{fvData.DataBytes[3]}]");
                        Console.WriteLine($"Recevie[{dd[0]}][{dd[1]}][{dd[2]}][{dd[3]}]");


                    }
                }
                Thread.Sleep(100);
                if (count++ > 10000)
                {
                    break;
                }
            }

            Thread.Sleep(10000);
            ch.StopReceiving();
            sNetPort.Stop();
        }

        /*
            string s = ":0.0      0x101   8       0x11 0x22 0x33 0x44 0x55 0x66 0x77:0.0  0x102   8       0xA1 0xB2 0xC3 0xD4 0xE5 0xF6 0x07";
            string[] sarr = s.Split(':');
            foreach (var a in sarr)
                Console.WriteLine(a);

        static void Main(string[] args)
        {
            // (1) IP 주소와 포트를 지정하고 TCP 연결 
            TcpClient tc = new TcpClient("127.0.0.1", 7000);
            //TcpClient tc = new TcpClient("localhost", 7000);

            string msg = "Hello World";
            byte[] buff = Encoding.ASCII.GetBytes(msg);

            // (2) NetworkStream을 얻어옴 
            NetworkStream stream = tc.GetStream();
            int a = 0;
            // (3) 스트림에 바이트 데이타 전송
            while(true)
            {

                // (4) 스트림으로부터 바이트 데이타 읽기
                byte[] outbuf = new byte[1024];
                int nbytes = stream.Read(outbuf, 0, outbuf.Length);
                string output = Encoding.ASCII.GetString(outbuf, 0, nbytes);
                Console.WriteLine($"{nbytes} bytes: {output}");
                Thread.Sleep(1000);
                if (a++ % 5 == 0)
                {
                    //    break;
                    stream.Write(buff, 0, buff.Length);
                }
            }
            // (5) 스트림과 TcpClient 객체 닫기
            stream.Close();
            tc.Close();
        }
        */
    }
}
