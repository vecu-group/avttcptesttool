﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TestTcpipServerUI
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        List<string> listLogData = new List<string>();
        TestServerThread serverThread;
        public MainWindow()
        {
            InitializeComponent();
            uiCbAutoCommSvr.IsChecked = true;
            uiCbStateRxPrint.IsChecked = false;            
            uiLbLogView.ItemsSource = listLogData;
            uiLbLogView.SelectionMode = SelectionMode.Single;
            serverThread = new TestServerThread();
            LogViewManager.LogMessageAdded += AddLog;
            UpdateUiStart(false);
        }

        private void Btn_Start_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
            {
                listLogData.Clear();
                uiLbLogView.Items.Refresh();
            }));
            serverThread.Start(uiCbAutoCommSvr.IsChecked, uiCbStateRxPrint.IsChecked);
            UpdateUiStart(true);
        }

        private void Btn_Stop_Click(object sender, RoutedEventArgs e)
        {
            serverThread.Stop();
            UpdateUiStart(false);
        }

        int test = 0;
        private void Btn_Test_Click(object sender, RoutedEventArgs e)
        {
            String ss = String.Format("[{0}] test log\nasdfassdfasdf\n", test++);
            LogViewManager.AddLogMessage("[test]", ss);
            LogViewManager.AddLogMessage("[test]", ss);
            LogViewManager.AddLogMessage("[test]", ss);
            LogViewManager.AddLogMessage("[test]", ss);
            LogViewManager.AddLogMessage("[test]", ss);
            LogViewManager.AddLogMessage("[test]", ss);
            LogViewManager.AddLogMessage("[test]", ss);
            LogViewManager.AddLogMessage("[test]", ss);
        }

        private void Btn_Clear_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
            {
                listLogData.Clear();
                uiLbLogView.Items.Refresh();
            }));

        }

        private void AddLog(string message)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
            {
                listLogData.Add(message);
                uiLbLogView.Items.Refresh();
                uiLbLogView.SelectedIndex = uiLbLogView.Items.Count - 1;
                //uiLbLogView.UpdateLayout();
                uiLbLogView.ScrollIntoView(uiLbLogView.SelectedItem);
            }));
        }

        private void Btn_StateTx_Click(object sender, RoutedEventArgs e)
        {
            serverThread.SendStateServer(uiTbStateTx.Text);
        }
        private void Btn_CommTx_Click(object sender, RoutedEventArgs e)
        {
            serverThread.SendCommServer(uiTbCommTx.Text);
        }

        void UpdateUiStart(bool isStart)
        {
            uiBtnStart.IsEnabled = !isStart;
            uiCbAutoCommSvr.IsEnabled = !isStart;


            uiBtnStop.IsEnabled = isStart;
            uiTbStateTx.IsEnabled = isStart;
            uiTbCommTx.IsEnabled = isStart;
            uiBtnStateTx.IsEnabled = isStart;
            uiBtnCommTx.IsEnabled = isStart;

        }
    }
}
