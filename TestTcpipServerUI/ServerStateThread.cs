﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Windows.Threading;
using System.IO;

namespace TestTcpipServerUI
{
    class ServerStateThread
    {
        TcpListener listener;
        TcpClient client;
        NetworkStream stream;
        const string LogTag = "ServerState";

        bool isRun = false;
        Thread rxThread;
        public bool? isAutoComm = false;
        public bool? isRxLog = true;

        public ServerStateThread()
        {
            listener = new TcpListener(IPAddress.Any, 11191);
        }
        public void Start()
        {
            Thread startThread = new Thread(new ThreadStart(RunServer));
            startThread.IsBackground = true;
            startThread.Start();
        }
        public void Stop()
        {
            isRun = false;

            if (stream != null)
                stream.Close();

            if (client != null)
                client.Close();

            if (listener != null)
                listener.Stop();
            LogViewManager.AddLogMessage(LogTag, "[Success] Stop Server");
        }


        void RunServer()
        {
            if (isRun)
            {
                LogViewManager.AddLogMessage(LogTag, "[Fail] It's already running.");
                return;
            }

            LogViewManager.AddLogMessage(LogTag, "Start Server (Waiting for client Connection)");
            listener.Start();
            try
            {
                client = listener.AcceptTcpClient();
            }
            catch
            {
                LogViewManager.AddLogMessage(LogTag, "Stopped waiting for connection.");
                return;
            }

            if (client != null)
            {
                IPEndPoint localEndpoint = (IPEndPoint)listener.LocalEndpoint;
                int serverPort = localEndpoint.Port;

                IPEndPoint ipPoint = (IPEndPoint)client.Client.RemoteEndPoint;
                LogViewManager.AddLogMessage(LogTag, $"Connection {ipPoint.Address.ToString()}({serverPort}) <=> {ipPoint.Port}");
                stream = client.GetStream();

                isRun = true;
                rxThread = new Thread(new ThreadStart(RunRx));
                rxThread.IsBackground = true;
                rxThread.Start();
                LogViewManager.AddLogMessage(LogTag, "[Success] Start Server");
            }
            else
            {
                LogViewManager.AddLogMessage(LogTag, "[Fail] Connect to comm server.");
            }
        }



        public void SendMsg(string msg)
        {
            if (isRun == false)
            {
                LogViewManager.AddLogMessage(LogTag, "[Fail] SendMsg isRun");
                return;
            }
            if (stream == null)
            {
                LogViewManager.AddLogMessage(LogTag, "[Fail] SendMsg stream null");
                return;
            }

            LogViewManager.AddLogMessage(LogTag, $"[Tx]{msg}");
            byte[] buff = Encoding.ASCII.GetBytes(msg);
            try
            {
                stream.Write(buff, 0, buff.Length);
            }
            catch (Exception e)
            {
                LogViewManager.AddLogMessage(LogTag, $"[Tx][Fail] {e.ToString()}");
            }
        }

        byte[] rxBuf = new byte[2048];
        void ReceiveMsg()
        {
            try
            {
                int nbytes = stream.Read(rxBuf, 0, rxBuf.Length);
                if (nbytes == 0)
                    return;
                string rxMsg = Encoding.ASCII.GetString(rxBuf, 0, nbytes);
                string[] arr = rxMsg.Split(';');
                foreach (string s in arr)
                {
                    if (s.Equals(""))
                        continue;
                    if (isRxLog == true)
                    {
                        string printS = String.Format($"[Rx]{s}");
                        LogViewManager.AddLogMessage(LogTag, printS);
                    }
                    if (isAutoComm == true && s.Equals("1,2,2,1"))
                    {
                        isAutoComm = false;
                        SendMsg("1,2,2,2;");
                    }
                }
            }
            catch (IOException ex)
            {
                LogViewManager.AddLogMessage(LogTag, "[Fail]" + ex.Message);
                Console.WriteLine("클라이언트 연결이 끊겼습니다: " + ex.Message);
                return;
            }
            catch (SocketException ex)
            {
                LogViewManager.AddLogMessage(LogTag, "[Fail]" + ex.Message);
                Console.WriteLine("소켓 예외 발생: " + ex.Message);
                return;
            }
        }

        void RunRx()
        {
            while (isRun)
            {
                if (client.Connected == false)
                    break;
                ReceiveMsg();
                Thread.Sleep(80);
            }
        }
    }
}
