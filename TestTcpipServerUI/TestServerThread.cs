﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Windows.Threading;
using System.IO;

namespace TestTcpipServerUI
{
    class TestServerThread
    {
        ServerCommThread serveComm;
        ServerStateThread serveState;
        const string LogTag = "Info";

        bool isStart;

        public TestServerThread()
        {
            serveComm = new ServerCommThread();
            serveState = new ServerStateThread();
            isStart = false;
        }
        public void Start(bool? isAutoCommSvr, bool? isStateRxLog)
        {
            if (isStart == true)
                return;
            LogViewManager.AddLogMessage(LogTag, "Start Thread");
            serveComm.Start();
            serveState.isAutoComm = isAutoCommSvr;
            serveState.isRxLog = isStateRxLog;
            serveState.Start();
            isStart = true;
        }
        public void Stop()
        {
            if (isStart == false)
                return;
            LogViewManager.AddLogMessage("[Info]", "Stop Thread");
            serveComm.Stop();
            serveState.Stop();
            isStart = false;
        }

        public void SendStateServer(string msg)
        {
            if (isStart == false)
                return;
            serveState.SendMsg(msg);
        }

        public void SendCommServer(string msg)
        {
            if (isStart == false)
                return;
            serveComm.SendMsg(msg);
        }
    }
}


/*

        void RunStart()
        {
            LogViewManager.AddLogMessage("[ServerState]", "Start Server (Waiting for client Connection)");
            listenerAvtState.Start();
            clientState = listenerAvtState.AcceptTcpClient();
            if(clientState != null)
            {
                IPEndPoint ipPoint = (IPEndPoint)clientState.Client.RemoteEndPoint;
                LogViewManager.AddLogMessage("[ServerState]", $"Connection {ipPoint.Address.ToString()}");
                streamState = clientState.GetStream();
            }
            else
            {
                LogViewManager.AddLogMessage("[ServerState]", "Failed to connect to comm server.");
            }
            listenerAvtComm.Start();
            client = listenerAvtComm.AcceptTcpClient();
            if (client != null)
            {
                IPEndPoint ipPoint = (IPEndPoint)client.Client.RemoteEndPoint;

                LogViewManager.AddLogMessage("[Info]", $"Connection {ipPoint.Address.ToString()}");
                streamClient = client.GetStream();

                runDataThread = true;
                txThread = new Thread(new ThreadStart(RunTx));
                txThread.IsBackground = true;
                txThread.Start();

                rxThread = new Thread(new ThreadStart(RunRx));
                rxThread.IsBackground = true;
                rxThread.Start();

                isStart = true;
                LogViewManager.AddLogMessage("[Info] Success Start Server");
            }
            else
            {
                LogViewManager.AddLogMessage("[Info] Fail Start Server");
            }
        }

        void RunEnd()
{
    LogViewManager.AddLogMessage("[Info] Release Server");
    listenerAvtComm.Stop();
    if (runDataThread)
    {
        runDataThread = false;
        txThread.Join(8000);
        rxThread.Join(8000);
        LogViewManager.AddLogMessage("[Info] Release TxRxThread");
    }
    isStart = false;

    if (streamClient != null)
    {
        streamClient.Close();
        streamClient = null;
    }
    if (client != null)
    {
        client.Close();
        client = null;
    }
    LogViewManager.AddLogMessage("[Info] Success Stop Server");
}

void SendMsg(string msg)
{
    if (runDataThread == false)
        return;
    if (streamClient == null)
        return;
    LogViewManager.AddLogMessage($"\t[Tx] {msg}");
    byte[] buff = Encoding.ASCII.GetBytes(msg);
    try
    {
        streamClient.Write(buff, 0, buff.Length);
    }
    catch (Exception e)
    {
        //LogViewManager.AddLogMessage($"[Error] {e.ToString()}");
        runDataThread = false;
        RunEnd();
    }
}
void RunTx()
{
    string msg1 = ":0x201,8,0x14 0x22 0x33 0x44 0x55 0x66 0x77 0x88";
    string msg2 = ":0x202,8,0xA1 0xB2 0xC3 0xD4 0xE5 0xF6 0x07 0x88";

    LogViewManager.AddLogMessage("[Info] Run RunTx");
    while (runDataThread)
    {
        if (client.Connected == false)
            break;
        SendMsg(msg1);
        //SendMsg(msg2);
        Thread.Sleep(100);
    }
}

byte[] rxBuf = new byte[2048];
void ReceiveMsg()
{
    try
    {
        int nbytes = streamClient.Read(rxBuf, 0, rxBuf.Length);
        if (nbytes == 0)
            return;
        string rxMsg = Encoding.ASCII.GetString(rxBuf, 0, nbytes);
        rxMsg = String.Format($"[{rxMsg}]");
        LogViewManager.AddLogMessage(rxMsg);
    }
    catch (IOException ex)
    {
        Console.WriteLine("클라이언트 연결이 끊겼습니다: " + ex.Message);
    }
    catch (SocketException ex)
    {
        Console.WriteLine("소켓 예외 발생: " + ex.Message);
    }
}

void RunRx()
{
    LogViewManager.AddLogMessage("[Info] Run RunRx");
    while (runDataThread)
    {
        if (client.Connected == false)
            break;
        ReceiveMsg();
        Thread.Sleep(80);
    }

}



*/